FROM elixir:1.12-slim AS builder
LABEL Author="Wenzel <wenzelraven@gmail.com>"

ARG MIX_ENV=prod
ARG NODE_VERSION=node_12.x
ARG NODE_ENV=production
ARG DEPLOY_ENV=staging

# Misc deps
RUN apt-get update \
    # To compile libsecp256k1 C binding for erlang NIFs 
    && apt-get install make gcc build-essential automake libtool inotify-tools autoconf git libgmp3-dev -y \ 
    && apt-get install apt-transport-https -y \
    && apt-get install build-essential -y \
    && apt-get install gnupg -y \
    && apt-get install curl -y

# Add Nodejs repo
RUN curl -fsSL https://deb.nodesource.com/setup_12.x | bash -
RUN apt-get update \
    && apt-get install nodejs -y \
    && apt-get install gawk -y \
    && mix local.rebar --force \
    && mix local.hex --force

# Project dependencies
WORKDIR /var/app/assets
COPY ./assets/package-lock.json /var/app/assets/package-lock.json
RUN npm install --production=false

ENV MIX_ENV $MIX_ENV

# Software deps
WORKDIR /var/app
COPY ./@openzeppelin /var/app/@openzeppelin
COPY ./config /var/app/config
COPY ./mix.exs /var/app/mix.exs
COPY ./mix.lock /var/app/mix.lock
RUN mix do deps.get, deps.compile

# Software
COPY ./start-${DEPLOY_ENV}.sh /var/app/start.sh
RUN chmod a+x /var/app/start.sh
COPY ./assets /var/app/assets
COPY ./lib /var/app/lib
COPY ./priv /var/app/priv
RUN chmod +x /var/app/priv/solc/ubuntu-0.8.10
RUN mkdir /var/app/priv/sol && mkdir /var/app/priv/abi

# Compile phoenix
WORKDIR /var/app
RUN mix do compile, phx.digest, release ${DEPLOY_ENV}

CMD ["./start.sh"]
