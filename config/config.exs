# This file is responsible for configuring your application
# and its dependencies with the aid of the Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.

# General application configuration
import Config

# config :greet_generators,
#   ecto_repos: [GreetGenerators.Repo]

# Configures the endpoint
config :greet_generators, GreetGeneratorsWeb.Endpoint,
  url: [host: "localhost"],
  render_errors: [view: GreetGeneratorsWeb.ErrorView, accepts: ~w(html json), layout: false],
  pubsub_server: GreetGenerators.PubSub,
  live_view: [signing_salt: "4QjwDPMX"]

# Configures the mailer
#
# By default it uses the "Local" adapter which stores the emails
# locally. You can see the emails in your browser, at "/dev/mailbox".
#
# For production it's recommended to configure a different adapter
# at the `config/runtime.exs`.
config :greet_generators, GreetGenerators.Mailer, adapter: Swoosh.Adapters.Local

# Solc compiler options
# All options supported for --combined-json:
#   abi,bin,bin-runtime,devdoc,function-debug,function-debug-runtime,generated-sources,generated-sources-runtime,
#   hashes,metadata,opcodes,srcmap,srcmap-runtime,storage-layout,userdoc
config :greet_generators, GreetGenerators.NFT,
  solc_opts: ["--optimize", "--overwrite", "--combined-json", "abi,bin,generated-sources"],
  solc_compiler: System.cwd() <> "/priv/solc/macos-0.8.10"

# Swoosh API client is needed for adapters other than SMTP.
config :swoosh, :api_client, false

# Configure esbuild (the version is required)
config :esbuild,
  version: "0.14.0",
  default: [
    args:
      ~w(js/app.js --bundle --target=es2017 --outdir=../priv/static/assets --external:/fonts/* --external:/images/*),
    cd: Path.expand("../assets", __DIR__),
    env: %{"NODE_PATH" => Path.expand("../deps", __DIR__)}
  ]

# Configures Elixir's Logger
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:request_id]

# Use Jason for JSON parsing in Phoenix
config :phoenix, :json_library, Jason

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
if System.get_env("DEPLOY_ENV") do
  import_config ~s[#{System.get_env("DEPLOY_ENV")}.exs]
else
  import_config "#{config_env()}.exs"
end
