#!/usr/bin/env bash

# Startup script for staging server.
PHX_HOST=build.demo.le7el.com PORT=80 RELEASE_NAME=staging PHX_SERVER=true /var/app/_build/prod/rel/staging/bin/staging start