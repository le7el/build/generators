defmodule GreetGeneratorsApi.ERC721View do
  use GreetGeneratorsApi, :view
  alias GreetGeneratorsApi.ERC721View

  def render("index.json", %{nft_erc721: nft_erc721}) do
    %{data: render_many(nft_erc721, ERC721View, "erc721.json")}
  end

  def render("show.json", %{erc721: erc721}) do
    %{data: render_one(erc721, ERC721View, "erc721.json")}
  end

  def render("erc721.json", %{erc721: erc721}) do
    %{
      id: erc721.id,
      name: erc721.name,
      symbol: erc721.symbol,
      description: erc721.description,
      total_supply: erc721.total_supply,
      chain_id: erc721.chain_id,
      mints_per_address: erc721.mints_per_address,
      minting_price: erc721.minting_price,
      minting_coin_address: erc721.minting_coin_address,
      token_uri: erc721.token_uri,
      whitelist_signer: erc721.whitelist_signer
    }
  end
end
