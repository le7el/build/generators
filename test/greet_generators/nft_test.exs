defmodule GreetGenerators.NFTTest do
  use GreetGenerators.DataCase

  alias GreetGenerators.NFT

  describe "nft_erc721" do
    import GreetGenerators.NFTFixtures

    test "create_erc721/1 with valid data creates a erc721" do
      valid_attrs = default_erc721()

      assert {:ok, source} = NFT.create_erc721(valid_attrs)
      # assert erc721.chain_id == 4
      # assert erc721.description == "Cool NFT project"
      # assert erc721.minting_coin_address == "0x0000000000000000000000000000000000000000"
      # assert erc721.minting_price == 0
      # assert erc721.mints_per_address == -1
      # assert erc721.name == "NFT Token"
      # assert erc721.symbol == "TKN"
      # assert erc721.token_uri == "some token_uri"
      # assert erc721.total_supply == 42
      # assert erc721.whitelist_signer == "0xee90eeccc941dcb67e175bc17fdd6c260a9eac9a"
    end

    test "create_erc721/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = NFT.create_erc721(default_erc721() |> Map.put(:whitelist_signer, "0x"))
    end
  end
end
